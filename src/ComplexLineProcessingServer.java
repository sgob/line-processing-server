import java.io.IOException;
import java.io.OutputStream;

public class ComplexLineProcessingServer extends SimpleLineProcessingServer {
    public ComplexLineProcessingServer(int port, String quitCommand, OutputStream os) {
        super(port, quitCommand, os);
    }

    public static void main(String[] args) throws IOException {
        ComplexLineProcessingServer clps = new ComplexLineProcessingServer(10000, "bye", System.out);
        clps.run();
    }

    @Override
    protected String process(String input) {
        if (input.equals("bye")) {
            return "bye";
        }

        String[] words = input.split("\\W+");
        return String.valueOf(words.length);
    }
}
